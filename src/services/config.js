import axios from 'axios'
export const http = axios.create({
    baseURL: 'https://heptashop-api.herokuapp.com/'
})